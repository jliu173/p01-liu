//
//  ViewController.m
//  P01-HelloWorld
//
//  Created by 刘江韵 on 17/1/21.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize message;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)CS580G:(id)sender
{
    [message setText:@"HEllo,world~~ I am Jiangyun Liu!"];
}

@end
