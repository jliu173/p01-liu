//
//  AppDelegate.h
//  P01-HelloWorld
//
//  Created by 刘江韵 on 17/1/21.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

